package com.common.service.impl;

import java.io.IOException;

import org.apache.commons.codec.binary.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.common.dao.AbstractCommonDAO;
import com.common.dao.BankDAO;
import com.common.responsemodel.Account;
import com.common.responsemodel.MoneyTrasnfer;
import com.common.responsemodel.Response;
import com.common.service.AccountService;
import com.common.utils.AbstractUtil;
import com.common.utils.Constansts;
import com.common.utils.Details;
import com.common.utils.ObjectThreadLocal;

/**
 * AccountServiceImpl for service accounts of bank
 * @author sanjay
 *
 */
public class AccountServiceImpl implements AccountService{
	/**
	 * HDfc Object
	 */
	private static BankDAO hdfcBank=new ObjectThreadLocal().getHdfcO();
	
	/**
	 * INg Object
	 */
	private static BankDAO ingBank=new ObjectThreadLocal().getIngO();
	
	private static final Logger LOG = LoggerFactory.getLogger(AbstractCommonDAO.class);
	
	/**
	 * AccountServiceImpl for service getBank
	 * @param moneyTrasnfer
	 * @return
	 */
	@Override
	public Response getBank(MoneyTrasnfer moneyTrasnfer) {
		Response result = null;
		Details details;
		try {
			result = new Response();
			details = new Details().getDetails(moneyTrasnfer);
			final Account fromBankAccount = moneyTrasnfer.getFromBankAccount();
			final Account toBankAccount = moneyTrasnfer.getToBankAccount();
			final  String amount = moneyTrasnfer.getAmount();
			result = transferMoney(details.getFromUrl(),fromBankAccount, details.getFromBank(),details.getToUrl(), toBankAccount,
					details.getTobank(),amount);
		} catch (IOException exception) {
			
			LOG.error(exception.getMessage()+"==================EXCEPTION=================="+exception.getCause());
			result.setMessage(exception.getMessage());
		}
		return result;
	}

	/**
	 * AccountServiceImpl for service transferMoney
	 * @param fromUrl
	 * @param fromBankAccount
	 * @param fromBank
	 * @param toUrl
	 * @param toBankAccount
	 * @param toBank
	 * @param amount
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	@Override
	public Response transferMoney(final String fromUrl,final Account fromBankAccount,final  BankDAO fromBank,final String toUrl,final  Account toBankAccount,final  BankDAO toBank,final String amount)
			throws ClientProtocolException, IOException {

		final Response validation = validation(fromUrl,fromBankAccount, fromBank, toUrl, toBankAccount, toBank,amount);
		final String message = validation.getMessage();
		if(!("".equals(message))) {
			return validation;
		}
		
		// removing money from bank
		final Response responseR =AbstractUtil.post(fromUrl,fromBankAccount, amount);
		
		if (responseR.getStatus()==200) {
			
			// adding money to bank
			final Response finalResponseR =AbstractUtil.post(toUrl,toBankAccount, amount);
			
			if (finalResponseR.getStatus()==200) {
				return new Response(200, Constansts.SUCCESS.getValue(),null);
			}
		}
		else {
			return new Response(responseR.getStatus(), responseR.getMessage(), null);
		}
		return new Response(500, Constansts.FAILED.getValue(), null);
	}

	private Response validation(final String fromUrl,final Account fromBankAccount,final  BankDAO fromBank,final String toUrl,final  Account toBankAccount,final  BankDAO toBank,final String amount) {
		String message="";
		if(fromUrl == null || fromUrl.equals("")) {
			message = "From Url";
		}
		if(amount == null || amount.equals("")) {
			message = "Amount is Blank";
		}
		if(fromBankAccount == null ) {
			message = "From Account Details Missing";
		}
		if(fromBank == null) {
			message = "From Bank Details Missing";
		}
		if(toUrl == null || toUrl.equals("")) {
			message = "To Url";
		}
		if(toBankAccount == null) {
			message = "To Account Details Missing";
		}
		if(toBank == null) {
			message = "To Bank Details Missing";
		}
		
		return new Response(500, message, null);
	}
	
	/**
	 * AccountServiceImpl for service addMoneyHdfc
	 * @param account
	 * @return
	 */
	@Override
	public Response addMoneyHdfc(final Account account) {
		final String message = hdfcBank.addMoney(account);
        return response(message);
	}
	/**
	 * AccountServiceImpl for service subtractMoneyHdfc
	 * @param account
	 * @return
	 */@Override
	public Response subtractMoneyHdfc(final Account account) {
		final String message = hdfcBank.subtractMoney(account);
        return response(message);
	}
	/**
	 * AccountServiceImpl for service addMoneyIng
	 * @param account
	 * @return
	 */
	 @Override
	public Response addMoneyIng(final Account account) {
		final String message =  ingBank.addMoney(account);
		return response(message);
	}
	/**
	 * AccountServiceImpl for service subtractMoneyIng
	 * @param account
	 * @return
	 */
	 @Override
	public Response subtractMoneyIng(final Account account) {
		final String message = ingBank.subtractMoney(account);
		return response(message);
	}

	/**
	 * 
	 * @param message
	 * @return
	 */
	private Response response(final String message) {
		if(message.equals(Constansts.SUCCESS.getValue())) {
			return new Response(200, message, null);
		}
        return new Response(500, message, null);
	}

	/**
	 * AccountServiceImpl for service getDetailsHdfc
	  * @return
	 *
	 */
	@Override
	public Response getDetailsHdfc() {
        return new Response(200, Constansts.SUCCESS.getValue(), AbstractUtil.toAccountList(hdfcBank.getAllAccounts()));
	}

	/**
	 * AccountServiceImpl for service getDetailsIng
	  * @return
	 *
	 */
	@Override
	public Response getDetailsIng() {
        return new Response(200, Constansts.SUCCESS.getValue(),AbstractUtil.toAccountList(ingBank.getAllAccounts()));
    }
}
