package com.common.service;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;

import com.common.dao.BankDAO;
import com.common.responsemodel.Account;
import com.common.responsemodel.MoneyTrasnfer;
import com.common.responsemodel.Response;

/**
 * AccountService for service accounts of bank
 * @author sanjay
 *
 */
public interface AccountService {

	/**
	 * AccountService for service getBank
	 * @return
	 *
	 */
	Response getBank(MoneyTrasnfer moneyTrasnfer);

	/**
	 * AccountService for service transferMoney
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	Response transferMoney(String fromUrl, Account fromBankAccount, BankDAO fromBank, String toUrl,
			Account toBankAccount, BankDAO toBank, String amount) throws ClientProtocolException, IOException;

	/**
	 * AccountService for service addMoneyHdfc
	 * @param account
	 * @return
	 */
	Response addMoneyHdfc(Account account);

	/**
	 * AccountService for service subtractMoneyHdfc
	 * @param account
	 * @return
	 */
	Response subtractMoneyHdfc(Account account);

	/**
	 * AccountService for service addMoneyIng
	 * @param account
	 * @return
	 */
	Response addMoneyIng(Account account);

	/**
	 * AccountService for service subtractMoneyIng
	 * @param account
	 * @return
	 */
	Response subtractMoneyIng(Account account);

	/**
	 * AccountService for service getDetailsHdfc
	  * @return
	 *
	 */
	Response getDetailsHdfc();

	/**
	 * AccountService for service getDetailsIng
	 * @return
	 */
	Response getDetailsIng();

}
