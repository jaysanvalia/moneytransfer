package com.common.model;

/**
 * Account DTO for data trasnfer Object
 * @author sanjay
 *
 */
public class AccountDTO implements Comparable<AccountDTO>{
 
	/**
	 * Account number
	 */
    private String no;
    
    /**
     * Account holder name
     */
    private String name;
    
    /**
     * Account balance
     */
    private Integer balance;
    
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public Integer getBalance() {
		return balance;
	}
	public void setBalance(Integer balance) {
		this.balance = balance;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public AccountDTO(){
		
	}
	public AccountDTO(String no, String name, Integer balance) {
		super();
		this.no = no;
		this.name = name;
		this.balance = balance;
	}
	@Override
	public String toString() {
		return "Account [no=" + no + ", name=" + name + ", balance=" + balance + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((no == null) ? 0 : no.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccountDTO other = (AccountDTO) obj;
		if (no == null) {
			if (other.no != null)
				return false;
		} else if (!no.equals(other.no))
			return false;
		return true;
	}
	@Override
	public int compareTo(AccountDTO o) {
		return this.getNo().compareTo(o.getNo());
	}
}
