package com.common.rest;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.common.responsemodel.Account;
import com.common.responsemodel.MoneyTrasnfer;
import com.common.responsemodel.Response;
import com.common.service.AccountService;
import com.common.utils.ObjectThreadLocal;

/**
 * MoneyTransferService rest service
 * @author sanjay
 *
 */
@Path("/bank")
public class MoneyTransferService {
	
	/**
	 * AccountService Obect holder for mutiple threads
	 */
	private static AccountService accountService=new ObjectThreadLocal().getAccountServiceO();
   
	/**
	 * MoneyTransferService rest service for moneyTransfer 
	 * @param moneyTrasnfer
	 * @return
	 */
	@POST
    @Path("/moneytransfer")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response moneyTransfer(final MoneyTrasnfer moneyTrasnfer) {
        return accountService.getBank(moneyTrasnfer);
    }
 
	/**
	 * MoneyTransferService rest service for getDetailsHdfc 
	 * @return
	 */
    @GET
    @Path("/hdfc")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getDetailsHdfc() {
        return accountService.getDetailsHdfc();
    }
    
    /**
	 * MoneyTransferService rest service for getDetailsIng 
	 * @return
	 */
    @GET
    @Path("/ing")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response getDetailsIng() {
        return accountService.getDetailsIng();
    }
    
    /**
  	 * MoneyTransferService rest service for subtractMoneyHdfc 
     * @param account
     * @return
     */
    @POST
    @Path("/hdfc/sub")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response subtractMoneyHdfc(final Account account) {
        return accountService.subtractMoneyHdfc(account);
    }
    
    /**
  	 * MoneyTransferService rest service for addMoneyIng 
     * @param account
     * @return
     */
    @POST
    @Path("/ing/add")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response addMoneyIng(final Account account) {
        return accountService.addMoneyIng(account);
    }
    
    /**
  	 * MoneyTransferService rest service for addMoneyHdfc 
     * @param account
     * @return
     */
    @POST
    @Path("/hdfc/add")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response addMoneyHdfc(final Account account) {
        return accountService.addMoneyHdfc(account);
    }
    
    /**
  	 * MoneyTransferService rest service for subtractMoneyIng 
     * @param account
     * @return
     */
    @POST
    @Path("/ing/sub")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response subtractMoneyIng(final Account account) {
        return accountService.subtractMoneyIng(account);
    }
 
}