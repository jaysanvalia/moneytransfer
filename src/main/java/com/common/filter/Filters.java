package com.common.filter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Stream;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.common.dao.AbstractCommonDAO;
import com.sun.jersey.core.util.ReaderWriter;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponse;
import com.sun.jersey.spi.container.ContainerResponseFilter;

/**
 * Filters for interceptor
 * @author sanjay
 *
 */
@Provider
public class Filters implements ContainerRequestFilter,ContainerResponseFilter{

	private static final Logger LOG = LoggerFactory.getLogger(Filters.class);
	/**
	 * request filter to intercept
	 */
	@Override
	public ContainerResponse filter(ContainerRequest request,ContainerResponse response) {
		LOG.info("==================Response==================");
		
		final MultivaluedMap<String, Object> httpHeaders = response.getHttpHeaders();
		final Set<Entry<String, List<Object>>> entrySet = httpHeaders.entrySet();
		final Stream<Entry<String, List<Object>>> stream = entrySet.stream();
		stream.forEach(t->{
			LOG.info("Key is==="+t.getKey()+"=======Value is===="+t.getValue());
		});
		LOG.info("==================Respone is=================="+response.getResponse().getEntity().toString());
		return response;
	}

	/**
	 * request filter to intercept
	 */
	@Override
	public ContainerRequest filter(ContainerRequest request) {
		LOG.info("==================Request==================");
		
		final MultivaluedMap<String, String> requestHeaders = request.getRequestHeaders();
		final Set<Entry<String, List<String>>> entrySet = requestHeaders.entrySet();
		final Stream<Entry<String, List<String>>> stream = entrySet.stream();
		stream.forEach(t->{
			LOG.info("Key is==="+t.getKey()+"=======Value is===="+t.getValue());
		});
		
		LOG.info("==================Request is=================="+getEntityBody(request));
		return request;
	}
	
	/**
	 * read reponse data
	 * @param requestContext
	 * @return
	 */
	private String getEntityBody(ContainerRequest requestContext)
    {
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		final InputStream in = requestContext.getEntityInputStream();
         
        final StringBuilder b = new StringBuilder();
        try
        {
            ReaderWriter.writeTo(in, out);
 
            byte[] requestEntity = out.toByteArray();
            if (requestEntity.length == 0)
            {
                b.append("").append("\n");
            }
            else
            {
                b.append(new String(requestEntity)).append("\n");
            }
            requestContext.setEntityInputStream(new ByteArrayInputStream(requestEntity) );
 
        } catch (IOException exception) {
        	
        	LOG.error(exception.getMessage()+"==================EXCEPTION=================="+exception.getCause());
        }
        return b.toString();
    }

}
