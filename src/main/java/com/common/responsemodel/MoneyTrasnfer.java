package com.common.responsemodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Repsonse MoneyTrasnfer
 * @author sanjay
 *
 */
@XmlRootElement(name = "MoneyTrasnfer")
@XmlAccessorType(XmlAccessType.FIELD)
public class MoneyTrasnfer implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Account fromBankAccount;
	private String fromBank;
	private String toBank;
	private Account toBankAccount;
	private String amount;
	
	public MoneyTrasnfer(Account fromBankAccount, String fromBank, String toBank, Account toBankAccount,
			String amount) {
		super();
		this.fromBankAccount = fromBankAccount;
		this.fromBank = fromBank;
		this.toBank = toBank;
		this.toBankAccount = toBankAccount;
		this.amount = amount;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public MoneyTrasnfer() {
	}
	public Account getFromBankAccount() {
		return fromBankAccount;
	}
	public void setFromBankAccount(Account fromBankAccount) {
		this.fromBankAccount = fromBankAccount;
	}
	public String getFromBank() {
		return fromBank;
	}
	public void setFromBank(String fromBank) {
		this.fromBank = fromBank;
	}
	public String getToBank() {
		return toBank;
	}
	public void setToBank(String toBank) {
		this.toBank = toBank;
	}
	public Account getToBankAccount() {
		return toBankAccount;
	}
	public void setToBankAccount(Account toBankAccount) {
		this.toBankAccount = toBankAccount;
	}
	@Override
	public String toString() {
		return "MoneyTrasnfer [fromBankAccount=" + fromBankAccount + ", fromBank=" + fromBank + ", toBank=" + toBank
				+ ", toBankAccount=" + toBankAccount + "]";
	}
	
	
	
}
