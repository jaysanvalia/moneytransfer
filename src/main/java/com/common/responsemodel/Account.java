package com.common.responsemodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Repsonse Account
 * @author sanjay
 *
 */
@XmlRootElement(name = "Account")
@XmlAccessorType(XmlAccessType.FIELD)
public class Account implements java.io.Serializable{
	/**
	 * serialized id
	 */
	private static final long serialVersionUID = 1L;

	public Account() {
	}

	public Account(String no, String name, String amount, String balance) {
		super();
		this.no = no;
		this.name = name;
		this.amount = amount;
		this.balance = balance;
	}

	private String no;
	private String name;
	private String amount;
	private String balance;

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "Account [no=" + no + ", name=" + name + ", amount=" + amount + "]";
	}

}
