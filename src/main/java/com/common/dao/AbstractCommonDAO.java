package com.common.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.common.model.AccountDTO;
import com.common.responsemodel.Account;
import com.common.utils.Constansts;

/**
 * Common DAO for All Banks
 * @author sanjay
 *
 */
public abstract class AbstractCommonDAO {
	
	private static final Logger LOG = LoggerFactory.getLogger(AbstractCommonDAO.class);
	 /**
     * add money to Account
     * @param account
     * @return
     */
	public String addMoney(final Account account) {
		String value;
		try {
			final AccountDTO accountDTO = getAccount(account.getNo());
			if(null == accountDTO) {
				return Constansts.ACCOUNT_NOT_FOUND.getValue();
			}
			final int balance = accountDTO.getBalance();
			final String amount = account.getAmount();
			accountDTO.setBalance(balance + Integer.parseInt(amount));
			value = Constansts.SUCCESS.getValue();
		} catch (Exception exception) {
			
			LOG.error(
					exception.getMessage() + "==================EXCEPTION==================" + exception.getCause());
			value = exception.getMessage();
		}
		return value;
	}

	
	/**
	 * get Account details
	 * @return
	 */
	protected abstract AccountDTO getAccount(String no);

	/**
	 * remoe money from Account
	 * @param account
	 * @return
	*/
	public String subtractMoney(Account account) {
		String value;
		try {
			final AccountDTO accountDTO = getAccount(account.getNo());
			if(null == accountDTO) {
				return Constansts.ACCOUNT_NOT_FOUND.getValue();
			}
			final Integer balance = accountDTO.getBalance();
			final String amount = account.getAmount();
			final int parseInt = Integer.parseInt(amount);
			LOG.debug("Account balance::"+balance+"   amount is::"+parseInt);
			if(parseInt > balance) {
				return Constansts.INSUFFICIENT_BALANCE.getValue();
			}
			accountDTO.setBalance(balance - parseInt);
			value = Constansts.SUCCESS.getValue();
		} catch (Exception exception) {
			
			LOG.error(
					exception.getMessage() + "==================EXCEPTION==================" + exception.getCause());
			value = exception.getMessage();
		}
		
		return value;
	}
}
