package com.common.dao;

import java.util.List;

import com.common.model.AccountDTO;
import com.common.responsemodel.Account;

/**
 * Bank interface for all Banks DAO
 * @author Sanjay
 *
 */
public interface BankDAO {
	
	/**
	 * get all Account details
	 * @return
	 */
    List<AccountDTO> getAllAccounts();
    
    /**
     * add money to Account
     * @param account
     * @return
     */
	String addMoney(Account account) ;
	
	/**
	 * remoe money from Account
	 * @param account
	 * @return
	*/
	String subtractMoney(Account account) ;
}
