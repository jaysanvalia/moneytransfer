package com.common.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.common.dao.AbstractCommonDAO;
import com.common.dao.BankDAO;
import com.common.model.AccountDTO;
import com.common.utils.DataStorage;

/**
 * ING bank DAO
 * @author sanjay
 *
 */
public class IngAccountDAOImpl extends AbstractCommonDAO implements BankDAO{
 
	/**
	 * get Account details
	 * @return
	 */
	@Override
    public AccountDTO getAccount(final String empNo) {
        return DataStorage.ING_MAP.get(empNo);
    }
 
    /**
	 * get all Account details
	 * @return
	 */
    @Override
    public List<AccountDTO> getAllAccounts() {
    	final Collection<AccountDTO> c = DataStorage.ING_MAP.values();
    	final List<AccountDTO> list = new ArrayList<>();
        list.addAll(c);
        return list;
    }
    
}