package com.common.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.common.dao.AbstractCommonDAO;
import com.common.dao.BankDAO;
import com.common.model.AccountDTO;
import com.common.utils.DataStorage;

/**
 * HDFC bank DAO
 * @author Sanjay
 *
 */
public class HdfcAccountDAOImpl extends AbstractCommonDAO implements BankDAO{
 
	/**
	 * get Account details
	 * @return
	 */
	@Override
    public AccountDTO getAccount(final String accountNo) {
        return DataStorage.HDFC_MAP.get(accountNo);
    }
 
    /**
	 * get all Account details
	 * @return
	 */
	@Override
    public List<AccountDTO> getAllAccounts() {
        final Collection<AccountDTO> collectionAccount = DataStorage.HDFC_MAP.values();
        final List<AccountDTO> listAccount = new ArrayList<>();
        listAccount.addAll(collectionAccount);
        return listAccount;
    }
     
}