package com.common.utils;

import com.common.dao.BankDAO;
import com.common.dao.impl.HdfcAccountDAOImpl;
import com.common.dao.impl.IngAccountDAOImpl;
import com.common.responsemodel.MoneyTrasnfer;

/**
 * Details for getting required Object based on request of money transfer
 * @author sanjay
 *
 */
public class Details {

	/**
	 * from bank details
	 */
	private BankDAO fromBank;
	/**
	 * to bank details
	 */
	private BankDAO tobank;
	/**
	 * from bank Url details
	 */
	private String fromUrl;
	/**
	 * to bank Url details
	 */
	private String toUrl;
	
	/**
	 * Param Constrcutor to create Object
	 * @param fromBank
	 * @param tobank
	 * @param fromUrl
	 * @param toUrl
	 */
	private Details(BankDAO fromBank, BankDAO tobank, String fromUrl, String toUrl) {
		this.fromBank = fromBank;
		this.tobank = tobank;
		this.fromUrl = fromUrl;
		this.toUrl = toUrl;
	}

	/**
	 * default Constructor
	 */
	public Details() {
	}

	public BankDAO getFromBank() {
		return fromBank;
	}

	public BankDAO getTobank() {
		return tobank;
	}

	public String getFromUrl() {
		return fromUrl;
	}

	public String getToUrl() {
		return toUrl;
	}

	/**
	 * getDetails return money transfer to and from details
	 * @param moneyTrasnfer
	 * @return
	 */
	public Details getDetails(final MoneyTrasnfer moneyTrasnfer){
		final String fromBank2 = moneyTrasnfer.getFromBank();
		final String toBank2 = moneyTrasnfer.getToBank();
		if (toBank2.equals(fromBank2)) {
			
			if (fromBank2.equals(Constansts.HDFC.getValue())) {
				fromBank = new HdfcAccountDAOImpl();
				tobank = new IngAccountDAOImpl();
				fromUrl=Constansts.HDFC_SUB.getValue();
				toUrl=Constansts.HDFC_ADD.getValue();
			} else {
				fromBank = new HdfcAccountDAOImpl();
				tobank = new IngAccountDAOImpl();
				fromUrl=Constansts.ING_SUB.getValue();
				toUrl=Constansts.ING_ADD.getValue();
			}

		} else {
			if (fromBank2.equals(Constansts.HDFC.getValue())) {
				fromBank = new HdfcAccountDAOImpl();
				tobank = new IngAccountDAOImpl();
				fromUrl=Constansts.HDFC_SUB.getValue();
				toUrl=Constansts.ING_ADD.getValue();
			} else {
				fromBank = new HdfcAccountDAOImpl();
				tobank = new IngAccountDAOImpl();
				fromUrl=Constansts.ING_SUB.getValue();
				toUrl=Constansts.HDFC_ADD.getValue();
			}
		}
		return new Details(fromBank, tobank, fromUrl, toUrl);
		
	}

	@Override
	public String toString() {
		return "Details [fromBank=" + fromBank + ", tobank=" + tobank + ", fromUrl=" + fromUrl + ", toUrl=" + toUrl
				+ "]";
	}
	
}
