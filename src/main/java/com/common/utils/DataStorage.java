package com.common.utils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.IntStream;

import com.common.model.AccountDTO;

public class DataStorage {
	/**
	 * HDFC Data Set
	 */
	public static final Map<String, AccountDTO> HDFC_MAP = new ConcurrentHashMap<String, AccountDTO>();
	
	/**
	 * Ing Data Set
	 */
	public static final Map<String, AccountDTO> ING_MAP = new ConcurrentHashMap<String, AccountDTO>();

	static {
    	initHdfc();
    	initIng();
    }
	
	/**
	 * initHdfc
	 */
	public static void initHdfc() {
		final AccountDTO acc1 = new AccountDTO("HDFC01", "Sanjay", 100);
		final AccountDTO acc2 = new AccountDTO("HDFC02", "Allen", 100);
		final AccountDTO acc3 = new AccountDTO("HDFC03", "Sanjay", 100);

		HDFC_MAP.put(acc1.getNo(), acc1);
		HDFC_MAP.put(acc2.getNo(), acc2);
		HDFC_MAP.put(acc3.getNo(), acc3);

		//createBulkData("HDFC0", HDFC_MAP);
		AbstractUtil.sortMap(HDFC_MAP);
	}

	/**
	 * initIng
	 */
	public static void initIng() {
		final AccountDTO acc1 = new AccountDTO("ING01", "Sanjay", 100);
		final AccountDTO acc2 = new AccountDTO("ING02", "Rajiv", 100);
		final AccountDTO acc3 = new AccountDTO("ING03", "Jone", 100);

		ING_MAP.put(acc1.getNo(), acc1);
		ING_MAP.put(acc2.getNo(), acc2);
		ING_MAP.put(acc3.getNo(), acc3);

		//createBulkData("ING0",ING_MAP);
		AbstractUtil.sortMap(ING_MAP);
	}

	private static void createBulkData(final String preFix,final Map<String, AccountDTO> ING_MAP2) {
		final IntStream range = IntStream.range(4, 10);
		range.forEach(t -> {
			final AccountDTO acc = new AccountDTO(preFix + t, "Sanjay" + t, 10000 + t);
			ING_MAP2.put(acc.getNo(), acc);
		});
	}
}
