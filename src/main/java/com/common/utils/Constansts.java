package com.common.utils;

/**
 * App Constants
 * @author sanjay
 *
 */
public enum Constansts {
	
	ING_ADD("http://localhost:8080/money/transfer/bank/ing/add"),
	ING_SUB("http://localhost:8080/money/transfer/bank/ing/sub"),
	HDFC_ADD("http://localhost:8080/money/transfer/bank/hdfc/add"),
	HDFC_SUB("http://localhost:8080/money/transfer/bank/hdfc/sub"),
	SUCCESS("Success"),
	FAILED("Failed"),
	ACCOUNT_NOT_FOUND("Account Not Found"),
	INSUFFICIENT_BALANCE("Insuffient Balance"), 
	HDFC("HDFC");
	
	Constansts(String value){
		this.value=value;
	}
	/**
	 * enum value
	 */
	String value;
	public String getValue() {
		return value;
	}
	
}
