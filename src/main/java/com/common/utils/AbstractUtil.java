package com.common.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.common.model.AccountDTO;
import com.common.responsemodel.Account;
import com.common.responsemodel.Response;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

/**
 * AbstractUtil for Application
 * @author sanjay
 *
 */
public abstract class AbstractUtil {

	/**
	 * Gson Object
	 */
	private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

	/**
	 * Obejct to toJson
	 * @param type
	 * @return
	 */
	public static <T> String toJson(final T type) {
		return gson.toJson(type);
	}

	/**
	 * Json to Object List
	 * @param json
	 * @param type
	 * @return
	 */
	public static <T> List<T> toPojoList(final String json,final  Class<T> type) {
		return gson.fromJson(json, new ListOfJson<T>(type));
	}
	
	/**
	 * Json to Object
	 * @param json
	 * @param type
	 * @return
	 */
	public static <T> T toPojo(final String json,final  Class<T> type) {
		return gson.fromJson(json, new TypeToken<Response>() {}.getType());
	}
	
	/**
	 * AccountDTO to Account
	 * @param accountDTO
	 * @return
	 */
	public static Account toAccount(final AccountDTO accountDTO) {
		return new Account(accountDTO.getNo(), accountDTO.getName(), "", String.valueOf(accountDTO.getBalance()));
	}

	/**
	 * Account to AccountDTO
	 * @param account
	 * @return
	 */
	public static AccountDTO toAccountDTO(final Account account) {
		return new AccountDTO(account.getNo(), account.getName(), Integer.parseInt(account.getBalance()));
	}

	/**
	 * AccountDTO list to Account list
	 * @param accountDTOList
	 * @return
	 */
	public static List<Account> toAccountList(final List<AccountDTO> accountDTOList) {
		final List<Account> list = new ArrayList<>();
		final Stream<AccountDTO> stream = accountDTOList.stream();
		stream.forEach(t -> {
			list.add(toAccount(t));
		});
		return list;
	}

	/**
	 * Account list to AccountDTO list
	 * @param accountList
	 * @return
	 */
	public static List<AccountDTO> toAccountDTOList(final List<Account> accountList) {
		final List<AccountDTO> list = new ArrayList<>();
		final Stream<Account> stream = accountList.stream();
		stream.forEach(t -> {
			list.add(toAccountDTO(t));
		});
		return list;
	}

	/**
	 * Post request Call
	 * @param url
	 * @param bankAccount
	 * @param amount
	 * @return
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	public static Response post(String url, Account bankAccount, String amount)
			throws UnsupportedEncodingException, IOException, ClientProtocolException {
		final CloseableHttpClient client = HttpClients.createDefault();
		bankAccount.setAmount(amount);
		final HttpPost httpPost = new HttpPost(url);
		httpPost.setEntity(new StringEntity(AbstractUtil.toJson(bankAccount)));
		httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("Content-type", "application/json");
		final CloseableHttpResponse response = client.execute(httpPost);
		final HttpEntity entity = response.getEntity();
		final String string = EntityUtils.toString(entity);
		final Response responseR =AbstractUtil.toPojo(string, Response.class);
		return responseR;
	}

	/**
	 * Sorting Map based on Key
	 * @param unsortMap
	 * @return
	 */
	public static <T> Map<String, T> sortMap(Map<String, T> unsortMap) {
		return unsortMap.entrySet().stream().sorted(Map.Entry.comparingByKey()).collect(Collectors
				.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));
	}

}

/**
 * ListOfJson for Converting List As type in type Parameter
 * @author sanjay
 *
 * @param <T>
 */
class ListOfJson<T> implements ParameterizedType {
	/**
	 * wrapper for type parameter like list 
	 */
	private Class<?> wrapped;

	/**
	 * wrapper for type parameter list 
	 * @param wrapper
	 */
	public ListOfJson(Class<T> wrapper) {
		this.wrapped = wrapper;
	}

	@Override
	public Type[] getActualTypeArguments() {
		return new Type[] { wrapped };
	}

	@Override
	public Type getRawType() {
		return List.class;
	}

	@Override
	public Type getOwnerType() {
		return null;
	}
}