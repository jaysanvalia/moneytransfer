package com.common.utils;

import com.common.dao.BankDAO;
import com.common.dao.impl.HdfcAccountDAOImpl;
import com.common.dao.impl.IngAccountDAOImpl;
import com.common.service.AccountService;
import com.common.service.impl.AccountServiceImpl;

/**
 * ObjectThreadLocal for Object creation and maintaining with threads
 * @author sanjay
 *
 */
public class ObjectThreadLocal {
	
	/**
	 * Object threadLocal for HDFC
	 */
	private static ThreadLocal<BankDAO> hdfcObject = new ThreadLocal<BankDAO>();
	
	/**
	 * Object threadLocal for ING
	 */
	private static ThreadLocal<BankDAO> ingObject = new ThreadLocal<BankDAO>();
	
	/**
	 * Object threadLocal for AccountService
	 */
	private static ThreadLocal<AccountService> accountServiceObject = new ThreadLocal<AccountService>();
	
	/**
	 * get Object for HDFC
	 * @return
	 */
	public BankDAO getHdfcO(){
		if(null == hdfcObject.get())
			hdfcObject.set(new HdfcAccountDAOImpl());
		return hdfcObject.get();
	}
	
	/**
	 * get Object for ING
	 * @return
	 */
	public BankDAO getIngO(){
		if(null == ingObject.get())
			ingObject.set(new IngAccountDAOImpl());
		return ingObject.get();
	}
	
	/**
	 * get Object for Account Service
	 * @return
	 */
	public AccountService getAccountServiceO(){
		if(null == accountServiceObject.get())
			accountServiceObject.set(new AccountServiceImpl());
		return accountServiceObject.get();
	}
}
