package com.common.test;

import java.util.Map;
import java.util.stream.IntStream;

import com.common.model.AccountDTO;
import com.common.responsemodel.Account;
import com.common.responsemodel.MoneyTrasnfer;

public class TestDataStorage {

	public static MoneyTrasnfer moneyTransferHdfcToIngAcc(){
		MoneyTrasnfer moneyTrasnfer =   new MoneyTrasnfer();
		Account account = new Account("HDFC01", "Sanjay", "", "");
		moneyTrasnfer.setAmount("100");
		moneyTrasnfer.setFromBank("HDFC");
		moneyTrasnfer.setFromBankAccount(account);

		account = new Account("ING03", "Jone", "", "");
		moneyTrasnfer.setToBank("ING");
		moneyTrasnfer.setToBankAccount(account);
		return moneyTrasnfer;
	}
	public static MoneyTrasnfer moneyTransferIngToHdfcAcc() {
		MoneyTrasnfer moneyTrasnfer =   new MoneyTrasnfer();
		Account account = new Account("ING03", "Jone", "", "");
		moneyTrasnfer.setFromBank("ING");
		moneyTrasnfer.setAmount("100");
		moneyTrasnfer.setFromBankAccount(account);
		
		moneyTrasnfer.setToBank("HDFC");
		account = new Account("HDFC01", "Sanjay", "", "");
		moneyTrasnfer.setToBankAccount(account);
		return moneyTrasnfer;
	}
	public static MoneyTrasnfer moneyTransferHdfcToHdfcAcc(){
		MoneyTrasnfer moneyTrasnfer = new MoneyTrasnfer();
		Account account = new Account("HDFC01", "Sanjay", "", "");
		moneyTrasnfer.setAmount("100");
		moneyTrasnfer.setFromBank("HDFC");
		moneyTrasnfer.setFromBankAccount(account);

		account = new Account("HDFC03", "Sanjay", "", "");
		moneyTrasnfer.setToBank("HDFC");
		moneyTrasnfer.setToBankAccount(account);
		return moneyTrasnfer;
	}
	
	public static MoneyTrasnfer moneyTransferIngToIngAcc() {
		MoneyTrasnfer moneyTrasnfer = new MoneyTrasnfer();
		Account account = new Account("ING03", "Jone", "", "");
		moneyTrasnfer.setAmount("100");
		moneyTrasnfer.setFromBank("ING");
		moneyTrasnfer.setFromBankAccount(account);

		account = new Account("ING01", "Sanjay", "", "");
		moneyTrasnfer.setToBank("ING");
		moneyTrasnfer.setToBankAccount(account);
		return moneyTrasnfer;
	}
	
	private static void createBulkDataForTransferIngToHdfc(String preFix, Map<String, AccountDTO> accmap2) {
		IntStream.range(4, 10).forEach(t -> {
			AccountDTO acc = new AccountDTO(preFix + t, "Sanjay" + t, 10000 + t);
			accmap2.put(acc.getNo(), acc);
		});
	}

	

	
}
