package com.common.test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.common.responsemodel.MoneyTrasnfer;
import com.common.utils.AbstractUtil;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Restfulltest {

	@Test
	public void moneyTransferHdfcToIngAcc() throws ClientProtocolException, IOException {
		MoneyTrasnfer moneyTrasnfer =  TestDataStorage.moneyTransferHdfcToIngAcc();
		
		CloseableHttpClient client = HttpClients.createDefault();

		commonPost(moneyTrasnfer, client);
	}
	
	@Test
	public void moneyTransferIngToHdfcAcc() throws ClientProtocolException, IOException {
		MoneyTrasnfer moneyTrasnfer =  TestDataStorage.moneyTransferIngToHdfcAcc();
		
		CloseableHttpClient client = HttpClients.createDefault();

		commonPost(moneyTrasnfer, client);
	}
	
	@Test
	public void moneyTransferHdfcToHdfcAcc() throws ClientProtocolException, IOException {
		final MoneyTrasnfer moneyTrasnfer = TestDataStorage.moneyTransferHdfcToHdfcAcc();
		final CloseableHttpClient client = HttpClients.createDefault();
		commonPost(moneyTrasnfer, client);
	}
	
	@Test
	public void moneyTransferIngToIngAcc() throws ClientProtocolException, IOException {
		final MoneyTrasnfer moneyTrasnfer = TestDataStorage.moneyTransferIngToIngAcc();
		final CloseableHttpClient client = HttpClients.createDefault();
		commonPost(moneyTrasnfer, client);
	}
	
	@Test
	public void zgetAccount() throws ClientProtocolException, IOException {
		long start = System.currentTimeMillis();
		
		//HDFC Account
		String string = getTest("http://localhost:8080/money/transfer/bank/hdfc");
		System.out.println("HDFC Accounts::::"+string);
		
		//ING Account
		string = getTest("http://localhost:8080/money/transfer/bank/ing");
		System.out.println("ING Accounts::::"+string);
		
		long end = System.currentTimeMillis();
		NumberFormat formatter = new DecimalFormat("#0.00000");
		System.out.print("Execution time is " + formatter.format((end - start) / 1000d) + " seconds");
	}

	@Test
	public void testBulkCases(){
		
		int numbeeOfThreads=30;
		int numberOfCalls=1000;
        final ExecutorService pool = Executors.newFixedThreadPool(numbeeOfThreads);
        final ExecutorCompletionService<String> completionService = new ExecutorCompletionService<>(pool);
        for (int i = 0; i < numberOfCalls; i++) {
            completionService.submit(new Callable<String>() {
                @Override
                public String call() throws Exception {
                    return bulk();
                }
            });
        }
        
        for(int i = 0; i < numberOfCalls; ++i) {
        	try {
        		final Future<String> future = completionService.take();
                System.out.println(future.get());
            } catch (ExecutionException | InterruptedException e) {
                System.err.println("Error while downloading"+e.getCause());
            }
        }
	}

	private String bulk() {
		try {
			MoneyTrasnfer moneyTrasnfer =  TestDataStorage.moneyTransferHdfcToIngAcc();
			CloseableHttpClient client = HttpClients.createDefault();
			commonPost(moneyTrasnfer, client);
			
			moneyTrasnfer =  TestDataStorage.moneyTransferIngToHdfcAcc();
			client = HttpClients.createDefault();
			commonPost(moneyTrasnfer, client);
			
			moneyTrasnfer = TestDataStorage.moneyTransferHdfcToHdfcAcc();
			client = HttpClients.createDefault();
			commonPost(moneyTrasnfer, client);
			
			moneyTrasnfer = TestDataStorage.moneyTransferIngToIngAcc();
			client = HttpClients.createDefault();
			commonPost(moneyTrasnfer, client);
			
			long start = System.currentTimeMillis();
			
			//HDFC Account
			String string = getTest("http://localhost:8080/money/transfer/bank/hdfc");
			System.out.println("HDFC Accounts::::"+string);
			
			//ING Account
			string = getTest("http://localhost:8080/money/transfer/bank/ing");
			System.out.println("ING Accounts::::"+string);
			
			long end = System.currentTimeMillis();
			NumberFormat formatter = new DecimalFormat("#0.00000");
			System.out.print("Execution time is " + formatter.format((end - start) / 1000d) + " seconds");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "Success";
	}

	private String getTest(String url) throws IOException, ClientProtocolException {
		HttpUriRequest request = new HttpGet(url);
		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
		final String string = EntityUtils.toString(httpResponse.getEntity());
		return string;
	}
	
	private void commonPost(MoneyTrasnfer moneyTrasnfer, CloseableHttpClient client)
			throws UnsupportedEncodingException, IOException, ClientProtocolException {
		// adding money to bank
		HttpPost httpPost = new HttpPost("http://localhost:8080/money/transfer/bank/moneytransfer");
		httpPost.setEntity(new StringEntity(AbstractUtil.toJson(moneyTrasnfer)));
		httpPost.setHeader("Accept", "application/json");
		httpPost.setHeader("Content-type", "application/json");
		CloseableHttpResponse response = client.execute(httpPost);

		// Then
		assertThat(response.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
		String string = EntityUtils.toString(response.getEntity());
		System.out.println("Response::::"+string);
	}
}
