# MoneyTransfer

MoneyTransfer - Revolut

Money Transfer is use to Transfer Money from one Bank to other Bank

Procedure to run as StandAlone Application is to run below command from project directory
java -jar target\MoneyTransfer.jar

Additional Commnad are in MoneyTrasnfer.txt

Below Rest API are

Url::http://{domain}:{port}/money/transfer/bank/moneyTransfer
Usage:: Money Transfer form one Bank Account to other Bank Account
Sample 
request:
{
  "fromBankAccount": {
    "no": "HDFC01",
    "name": "Sanjay",
    "amount": "",
    "balance": ""
  },
  "fromBank": "HDFC",
  "toBank": "ING",
  "toBankAccount": {
    "no": "ING03",
    "name": "Jone",
    "amount": "",
    "balance": ""
  },
  "amount": "100"
}
Response:
{"status":"200","message":"Success","data":[{"no":"HDFC02","name":"Allen","amount":"","balance":"2000"},{"no":"HDFC01","name":"Sanjay","amount":"","balance":"900"},{"no":"HDFC03","name":"Sanjay","amount":"","balance":"3000"}]}
{"status":"200","message":"Success","data":[{"no":"ING01","name":"Sanjay","amount":"","balance":"4000"},{"no":"ING03","name":"Jone","amount":"","balance":"8100"},{"no":"ING02","name":"Rajiv","amount":"","balance":"7000"}]}
Response::::{"status":"200","message":"Success"}


Url::http://{domain}:{port}/money/transfer/bank/hdfc/add
Usage:: adding money to hdfc Bank Account

Url::http://{domain}:{port}/money/transfer/bank/ing/add
Usage:: adding money to ing Bank Account


Url::http://{domain}:{port}/money/transfer/bank/hdfc/sub
Usage:: deducting money from hdfc Bank Account

Url::http://{domain}:{port}/money/transfer/bank/ing/sub
Usage:: deducting money from ing Bank Account


Url::http://{domain}:{port}/money/transfer/bank/hdfc
Usage:: Getting Bank Account details of hdfc

Url::http://{domain}:{port}/money/transfer/bank/ing
Usage:: Getting Bank Account details of ing


Below are Test Cases
BEFORE
{"status":"200","message":"Success","data":[{"no":"ING01","name":"Sanjay","amount":"","balance":"4000"},{"no":"ING03","name":"Jone","amount":"","balance":"8000"},{"no":"ING02","name":"Rajiv","amount":"","balance":"7000"}]}

{"status":"200","message":"Success","data":[{"no":"HDFC02","name":"Allen","amount":"","balance":"2000"},{"no":"HDFC01","name":"Sanjay","amount":"","balance":"1000"},{"no":"HDFC03","name":"Sanjay","amount":"","balance":"3000"}]}


AFTER test case
method::moneyTransferHdfcToIngAcc
method::moneyTransferIngToHdfcAcc
{"status":"200","message":"Success","data":[{"no":"HDFC02","name":"Allen","amount":"","balance":"2000"},{"no":"HDFC01","name":"Sanjay","amount":"","balance":"900"},{"no":"HDFC03","name":"Sanjay","amount":"","balance":"3000"}]}
{"status":"200","message":"Success","data":[{"no":"ING01","name":"Sanjay","amount":"","balance":"4000"},{"no":"ING03","name":"Jone","amount":"","balance":"8100"},{"no":"ING02","name":"Rajiv","amount":"","balance":"7000"}]}
Response::::{"status":"200","message":"Success"}



method::moneyTransferHdfcToHdfcAcc
method::moneyTransferIngToIngAcc
Response::::{"status":"200","message":"Success"}
{"status":"200","message":"Success","data":[{"no":"ING01","name":"Sanjay","amount":"","balance":"4000"},{"no":"ING03","name":"Jone","amount":"","balance":"8100"},{"no":"ING02","name":"Rajiv","amount":"","balance":"7000"}]}
{"status":"200","message":"Success","data":[{"no":"HDFC02","name":"Allen","amount":"","balance":"2000"},{"no":"HDFC01","name":"Sanjay","amount":"","balance":"800"},{"no":"HDFC03","name":"Sanjay","amount":"","balance":"3100"}]}



method::zgetAccount
HDFC Accounts::::{"status":"200","message":"Success","data":[{"no":"HDFC02","name":"Allen","amount":"","balance":"2000"},{"no":"HDFC01","name":"Sanjay","amount":"","balance":"800"},{"no":"HDFC06","name":"Sanjay6","amount":"","balance":"10006"},{"no":"HDFC05","name":"Sanjay5","amount":"","balance":"10005"},{"no":"HDFC04","name":"Sanjay4","amount":"","balance":"10104"},{"no":"HDFC03","name":"Jones","amount":"","balance":"3000"},{"no":"HDFC09","name":"Sanjay9","amount":"","balance":"10009"},{"no":"HDFC08","name":"Sanjay8","amount":"","balance":"10008"},{"no":"HDFC07","name":"Sanjay7","amount":"","balance":"10007"}]}
ING Accounts::::{"status":"200","message":"Success","data":[{"no":"ING07","name":"Sanjay7","amount":"","balance":"10007"},{"no":"ING06","name":"Sanjay6","amount":"","balance":"10006"},{"no":"ING09","name":"Sanjay9","amount":"","balance":"10009"},{"no":"ING08","name":"Sanjay8","amount":"","balance":"10008"},{"no":"ING01","name":"Sanjay","amount":"","balance":"4100"},{"no":"ING03","name":"James","amount":"","balance":"8000"},{"no":"ING02","name":"Rajiv","amount":"","balance":"7000"},{"no":"ING05","name":"Sanjay5","amount":"","balance":"10005"},{"no":"ING04","name":"Sanjay4","amount":"","balance":"10004"}]}
Execution time is 0.07900 seconds

method::testBulkCases
Usage::used to test bulk requests
executed without error